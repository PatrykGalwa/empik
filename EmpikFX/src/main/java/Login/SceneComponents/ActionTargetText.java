package Login.SceneComponents;

import javafx.scene.text.Text;

class ActionTargetText extends Text {
    private static ActionTargetText actionTargetText;

    private ActionTargetText() {
    }

    public static ActionTargetText getInstance() {
        if (actionTargetText == null) {
            actionTargetText = new ActionTargetText();
        }
        return actionTargetText;
    }
}
