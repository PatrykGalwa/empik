package Login.SceneComponents;

import javafx.scene.control.TextField;

class UserTextField extends TextField {
    private static UserTextField userTextField;

    private UserTextField() {

    }

    public static UserTextField getInstance() {
        if (userTextField == null) {
            userTextField = new UserTextField();
        }
        return userTextField;
    }
}
