package Login.SceneComponents;

import javafx.scene.control.PasswordField;

class PasswordFieldBox extends PasswordField {
    private static PasswordFieldBox passwordFieldBox;

    private PasswordFieldBox() {

    }

    public static PasswordFieldBox getInstance() {
        if (passwordFieldBox == null) {
            passwordFieldBox = new PasswordFieldBox();
        }
        return passwordFieldBox;
    }
}
