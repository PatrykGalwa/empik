package Login.SceneComponents;

import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class MainGridPane extends GridPane {

    public MainGridPane(Stage primaryStage) {
        this.setAlignment(Pos.CENTER);
        this.setHgap(10);
        this.setVgap(10);
        this.setPadding(new Insets(25, 25, 25, 25));
        this.add(new SceneTitle(), 0, 0, 2, 1);
        this.add(new UserNameLabel(), 0, 1);
        this.add(UserTextField.getInstance(), 1, 1);
        this.add(new PasswordLabel(), 0, 2);
        this.add(PasswordFieldBox.getInstance(), 1, 2);
        this.add(new SignInButtonHBox(primaryStage), 1, 4);
        this.add(ActionTargetText.getInstance(), 1, 6);

    }
}
