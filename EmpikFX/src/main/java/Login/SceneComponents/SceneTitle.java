package Login.SceneComponents;

import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

class SceneTitle extends Text{

    public SceneTitle() {
        this.setText("Welcome");
        this.setFont(Font.font("Tahoma", FontWeight.NORMAL, 20));
    }
}
