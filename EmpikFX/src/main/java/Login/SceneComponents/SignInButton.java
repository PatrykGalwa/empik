package Login.SceneComponents;

import MainStage.FXMain;
import com.company.Repository.csv.Database.DBUtil;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

import static javafx.application.Platform.exit;

class SignInButton extends Button {

    public SignInButton(Stage primaryStage) {
        this.setText("Sign in");
        this.setOnAction(new EventHandler<ActionEvent>() {
            int count = 0;

            @Override
            public void handle(ActionEvent event) {
                if (DBUtil.getInstance().checkPassword(UserTextField.getInstance().getCharacters().toString(),
                        PasswordFieldBox.getInstance().getCharacters().toString())) {
                    new FXMain().start(primaryStage);
                } else {
                    count++;
                    ActionTargetText.getInstance().setText("Wrong login or password");
                    ActionTargetText.getInstance().setFill(Color.FIREBRICK);
                    if (count >= 3) {
                        exit();
                    }
                }



            }
        });
    }
}
