package Login.SceneComponents;

import javafx.geometry.Pos;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;

class SignInButtonHBox extends HBox {

    public SignInButtonHBox(Stage primaryStage) {
        this.setSpacing(10);
        this.setAlignment(Pos.BOTTOM_RIGHT);
        this.getChildren().add(new SignInButton(primaryStage));
    }
}
