package Login;

import Login.SceneComponents.MainGridPane;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;



public class LoginMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("Login Form");
        MainGridPane grid = new MainGridPane(primaryStage);
        Scene scene = new Scene(grid);
        primaryStage.setScene(scene);
        primaryStage.show();
    }
}
