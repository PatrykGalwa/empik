package MainStage.Function;

import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;

public class FunctionHBox extends HBox {

    public FunctionHBox(VBox warehouseFunction, VBox traderFunction, VBox administrationFunction) {
        this.getChildren().add(new WarehouseFunctionButton(warehouseFunction));
        this.getChildren().add(new TraderFunctionButton(traderFunction));
        this.getChildren().add(new AdministrationFunctionButton(administrationFunction));
    }
}
