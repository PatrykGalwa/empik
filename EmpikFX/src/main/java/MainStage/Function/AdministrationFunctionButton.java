package MainStage.Function;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class AdministrationFunctionButton extends Button {

    public AdministrationFunctionButton(VBox vBox) {
        this.setText("Administration Function");
        this.setOnAction(actionEvent -> {
            if (vBox.isVisible()) {
                vBox.setVisible(false);
            } else {
                vBox.setVisible(true);
            }
        });
    }
}
