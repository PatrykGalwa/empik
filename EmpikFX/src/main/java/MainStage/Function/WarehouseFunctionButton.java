package MainStage.Function;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class WarehouseFunctionButton extends Button {

    public WarehouseFunctionButton(VBox vBox) {
        this.setText("Warehouse Function");
        this.setOnAction(eventHandler -> {
            if (vBox.isVisible()) {
                vBox.setVisible(false);
            } else {
                vBox.setVisible(true);
            }
        });
    }
}
