package MainStage.Function;

import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

public class TraderFunctionButton extends Button {

    public TraderFunctionButton(VBox vBox) {
        this.setText("Trader Function");
        this.setOnAction(eventHandler -> {
            if (vBox.isVisible()) {
                vBox.setVisible(false);
            } else {
                vBox.setVisible(true);
            }
        });
    }
}
