package MainStage.Function.FunctionAction.AdministrationFunctionAction.AdministrationFunctionActionButtons;

import javafx.scene.layout.HBox;

public class FunctionActionAdministrationButtonsHBox extends HBox {

    public FunctionActionAdministrationButtonsHBox() {
        this.getChildren().add(new InvoicesAdministrationButton());
        this.getChildren().add(new IncomeButton());
        this.getChildren().add(new CostOfGettingIncomeButton());
    }
}
