package MainStage.Function.FunctionAction.AdministrationFunctionAction.AdministrationFunctionActionButtons;

import javafx.scene.control.Button;

public class CostOfGettingIncomeButton extends Button {

    public CostOfGettingIncomeButton() {
        this.setText("Cost of getting income");
    }
}
