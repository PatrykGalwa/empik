package MainStage.Function.FunctionAction.AdministrationFunctionAction;

import MainStage.Function.FunctionAction.AdministrationFunctionAction.AdministrationFunctionActionButtons.FunctionActionAdministrationButtonsHBox;
import javafx.scene.layout.VBox;

public class AdministrationFunctionActionVBox extends VBox {

    public AdministrationFunctionActionVBox() {
        this.getChildren().add(new FunctionActionAdministrationButtonsHBox());
        this.setVisible(false);
    }
}
