package MainStage.Function.FunctionAction.TraderFunctionAction.TraderFunctionActionButtons;

import javafx.scene.layout.HBox;

public class FunctionActionTraderButtonsHBox extends HBox {

    public FunctionActionTraderButtonsHBox() {
        this.getChildren().add(new InvoicesTraderButton());
        this.getChildren().add(new ReceiptButton());
        this.getChildren().add(new ReservationsButton());
    }
}
