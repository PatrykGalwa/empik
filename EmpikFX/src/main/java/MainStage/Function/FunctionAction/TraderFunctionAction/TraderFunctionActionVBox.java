package MainStage.Function.FunctionAction.TraderFunctionAction;

import MainStage.Function.FunctionAction.TraderFunctionAction.TraderFunctionActionButtons.FunctionActionTraderButtonsHBox;
import javafx.scene.layout.VBox;

public class TraderFunctionActionVBox extends VBox {

    public TraderFunctionActionVBox() {
        this.getChildren().add(new FunctionActionTraderButtonsHBox());
        this.setVisible(false);
    }
}
