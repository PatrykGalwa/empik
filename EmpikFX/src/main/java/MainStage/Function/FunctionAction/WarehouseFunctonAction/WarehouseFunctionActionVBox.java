package MainStage.Function.FunctionAction.WarehouseFunctonAction;

import MainStage.Function.FunctionAction.WarehouseFunctonAction.WarehouseFunctionActionButtons.FunctionActionWarehouseButtonsHBox;
import javafx.scene.layout.VBox;

public class WarehouseFunctionActionVBox extends VBox {

    public WarehouseFunctionActionVBox() {
        this.getChildren().add(new FunctionActionWarehouseButtonsHBox());
        this.setVisible(false);
    }
}
