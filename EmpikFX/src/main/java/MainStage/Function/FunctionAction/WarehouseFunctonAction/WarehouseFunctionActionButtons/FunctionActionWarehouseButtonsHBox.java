package MainStage.Function.FunctionAction.WarehouseFunctonAction.WarehouseFunctionActionButtons;

import javafx.scene.layout.HBox;

public class FunctionActionWarehouseButtonsHBox extends HBox {

    public FunctionActionWarehouseButtonsHBox() {
        this.getChildren().add(new IntakeButton());
        this.getChildren().add(new LossButton());
        this.getChildren().add(new ResourcesButton());
    }
}
