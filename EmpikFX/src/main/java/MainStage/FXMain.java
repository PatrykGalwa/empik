package MainStage;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FXMain extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        Scene mainScene = new Scene(new MainVBox());
        primaryStage.setMinHeight(480);
        primaryStage.setMinWidth(640);
        primaryStage.setScene(mainScene);
        primaryStage.setTitle("Shop");
        primaryStage.show();
    }
}
