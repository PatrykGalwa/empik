package MainStage.MainButtons;

import javafx.scene.control.Button;

class PreferencesButton extends Button {
    private static PreferencesButton preferencesButton;

    private PreferencesButton() {
        this.setText("Preferences");
        new Setter(this);
    }

    public static PreferencesButton getInstance() {
        if (preferencesButton == null) {
            preferencesButton = new PreferencesButton();
        }
        return preferencesButton;
    }
}
