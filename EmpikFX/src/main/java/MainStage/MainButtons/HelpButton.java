package MainStage.MainButtons;

import javafx.scene.control.Button;

class HelpButton extends Button {
    private static HelpButton helpButton;

    private HelpButton() {
        this.setText("Help");
        new Setter(this);
    }

    public  static HelpButton getInstance() {
        if (helpButton == null) {
            helpButton = new HelpButton();
        }
        return helpButton;
    }
}
