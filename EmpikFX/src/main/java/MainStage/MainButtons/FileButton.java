package MainStage.MainButtons;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.control.Button;

class FileButton extends Button {
    private  static FileButton fileButton;

    private FileButton() {
        this.setText("File");
        new Setter(this);
        this.setOnAction(new EventHandler<ActionEvent>() {
            @Override
            public void handle(ActionEvent event) {

            }
        });
    }

    public static FileButton getInstance() {
        if (fileButton == null) {
            fileButton = new FileButton();
        }
        return fileButton;
    }


}
