package MainStage.MainButtons;

import javafx.geometry.Insets;
import javafx.scene.layout.HBox;

public class ButtonsHBox extends HBox {
    private static ButtonsHBox buttonsHBox;

    private ButtonsHBox() {
        this.setSpacing(2);
        this.setPadding(new Insets(0, 0, 2, 0));
        this.getChildren().add(FileButton.getInstance());
        this.getChildren().add(EditButton.getInstance());
        this.getChildren().add(PreferencesButton.getInstance());
        this.getChildren().add(HelpButton.getInstance());
    }

    public static ButtonsHBox getInstance() {
        if (buttonsHBox == null) {
            buttonsHBox = new ButtonsHBox();
        }
        return buttonsHBox;
    }
}
