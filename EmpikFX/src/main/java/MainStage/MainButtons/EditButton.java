package MainStage.MainButtons;

import javafx.scene.control.Button;

class EditButton extends Button {
    private static EditButton editButton;

    private EditButton() {
        this.setText("Edit");
        new Setter(this);
    }

    public static EditButton getInstance() {
        if (editButton == null) {
            editButton = new EditButton();
        }
        return editButton;
    }
}
