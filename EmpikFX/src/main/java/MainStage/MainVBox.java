package MainStage;

import MainStage.MainButtons.ButtonsHBox;
import MainStage.Function.FunctionHBox;
import MainStage.Function.FunctionAction.AdministrationFunctionAction.AdministrationFunctionActionVBox;
import MainStage.Function.FunctionAction.TraderFunctionAction.TraderFunctionActionVBox;
import MainStage.Function.FunctionAction.WarehouseFunctonAction.WarehouseFunctionActionVBox;
import javafx.scene.layout.VBox;

public class MainVBox extends VBox {
    private AdministrationFunctionActionVBox administrationFunctionActionVBox;
    private TraderFunctionActionVBox traderFunctionActionVBox;
    private WarehouseFunctionActionVBox warehouseFunctionActionVBox;

    public MainVBox() {
        administrationFunctionActionVBox = new AdministrationFunctionActionVBox();
        traderFunctionActionVBox = new TraderFunctionActionVBox();
        warehouseFunctionActionVBox = new WarehouseFunctionActionVBox();
        this.setSpacing(2);
        this.getChildren().add(ButtonsHBox.getInstance());
        this.getChildren().add(new FunctionHBox(warehouseFunctionActionVBox, traderFunctionActionVBox, administrationFunctionActionVBox));
    }
}
