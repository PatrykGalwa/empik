package com.company.Repository.api.nbp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class ExchangeRatesParsingTest {

    private File jsonToParse = new File("src/main/resources/ExchangeRates.json");

    @Test
    void shouldParseJsonInProperWay() throws IOException {
        ObjectMapper mapper = new ObjectMapper();
        List<ExchangeRates> exchangeRatesList = mapper
                .readValue(jsonToParse, new TypeReference<List<ExchangeRates>>(){});
        System.out.println(exchangeRatesList);
    }
}