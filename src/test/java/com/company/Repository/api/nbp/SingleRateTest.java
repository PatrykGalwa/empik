package com.company.Repository.api.nbp;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;

class SingleRateTest {

    private String jsonToParse = "{\n" +
            "        \"currency\":\"korona szwedzka\",\n" +
            "        \"code\":\"SEK\",\n" +
            "        \"bid\":0.4086,\n" +
            "        \"ask\":0.4168\n" +
            "      }";

    @Test
    void test() throws IOException {
        ObjectMapper objectMapper = new ObjectMapper();
        SingleRate singleRate = objectMapper.readValue(jsonToParse, SingleRate.class);
        System.out.println(singleRate.toString());
    }
}