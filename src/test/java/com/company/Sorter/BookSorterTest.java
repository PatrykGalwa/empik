package com.company.Sorter;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.AudioAlbum.Track;
import com.company.Container.Book.Book;
import com.company.Container.Book.EBook;
import com.company.Container.Book.PaperBook;
import com.company.Cart;
import com.company.Container.Poster.Poster;
import com.company.Sorter.BookSorter;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class BookSorterTest {
    private Cart cart;
    private final List<Track> list = new ArrayList<>();
    private final List<String> supportedFormats = new ArrayList<>();
    private final LocalDate date = LocalDate.now();
    private final String author = "Ola";
    private final BigDecimal bigDecimal = new BigDecimal(100);
    private final int page = 100;
    private final AudioAlbum audioAlbum = new AudioAlbum(1, "lklk", bigDecimal, date, list);
    private final AudioAlbum audioAlbum1 = new AudioAlbum(2, "lklk", bigDecimal, date, list);
    private final AudioAlbum audioAlbum2 = new AudioAlbum(3, "lklk", bigDecimal, date, list);
    private final PaperBook book = new PaperBook(1, "alala", bigDecimal, author, 200);
    private final PaperBook book1 = new PaperBook(1, "alala", bigDecimal, author, 150);
    private final PaperBook book2 = new PaperBook(1, "alala", bigDecimal, author, page);
    private final EBook book3 = new EBook(4, "lalala", bigDecimal, author, 250, supportedFormats);
    private final EBook ebook = new EBook(5, "lklk", bigDecimal, "ewa", 300, supportedFormats);
    private final Poster poster = new Poster(1, "lklk", bigDecimal);
    private final Poster poster1 = new Poster(1, "lklk", bigDecimal);
    private final Poster poster2 = new Poster(1, "lklk", bigDecimal);

    @Test
    @DisplayName("Product list given then should return Books list.")
    public void shouldFilterProductsByBooks() {
        cart = new Cart();

        cart.addProduct(audioAlbum);
        cart.addProduct(audioAlbum1);
        cart.addProduct(audioAlbum2);
        cart.addProduct(book);
        cart.addProduct(book1);
        cart.addProduct(book2);
        cart.addProduct(poster);
        cart.addProduct(poster1);
        cart.addProduct(poster2);

        List<Book> booksList = new BookSorter().filterProductsByBooks(cart.getProductsBill());
        List<Book> expected = Arrays.asList(book, book1, book2);

        assertEquals(expected, booksList);
    }

    @Test
    @DisplayName("Should return list with books with one author")
    public void shouldFilterProductsByBooksAndAuthor() {
        cart = new Cart();

        cart.addProduct(audioAlbum);
        cart.addProduct(audioAlbum1);
        cart.addProduct(audioAlbum2);
        cart.addProduct(book);
        cart.addProduct(book1);
        cart.addProduct(book2);
        cart.addProduct(poster);
        cart.addProduct(poster1);
        cart.addProduct(poster2);
        cart.addProduct(book3);
        cart.addProduct(ebook);

        List<Book> booksList = new BookSorter().filterProductsByBooksAndAuthor(cart.getProductsBill(), author);
        List<Book> expected = Arrays.asList(book, book1, book2, book3);

        assertEquals(expected, booksList);
    }

    @Test
    @DisplayName("Should return books list sorted by pages")
    public void shouldReturnBooksListSortedByPages() {
        cart = new Cart();

        cart.addProduct(audioAlbum);
        cart.addProduct(audioAlbum1);
        cart.addProduct(audioAlbum2);
        cart.addProduct(book);
        cart.addProduct(book1);
        cart.addProduct(book2);
        cart.addProduct(poster);
        cart.addProduct(poster1);
        cart.addProduct(poster2);
        cart.addProduct(book3);
        cart.addProduct(ebook);

        List<Book> booksList = new BookSorter().filterProductsByBooksAndSortedByLength(cart.getProductsBill());
        List<Book> expected = Arrays.asList(book2, book1, book, book3, ebook);

        assertEquals(expected, booksList);
    }

    @Test
    @DisplayName("Should return product list filtered by books, author and sorted by lenght")

    public void shouldReturnProductListFilteredByBooksAuthorAndSortedByLength() {
        cart = new Cart();

        cart.addProduct(audioAlbum);
        cart.addProduct(audioAlbum1);
        cart.addProduct(audioAlbum2);
        cart.addProduct(book);
        cart.addProduct(book1);
        cart.addProduct(book2);
        cart.addProduct(poster);
        cart.addProduct(poster1);
        cart.addProduct(poster2);
        cart.addProduct(book3);
        cart.addProduct(ebook);

        List<Book> books = new BookSorter().filterProductsByBookAuthorAndSortedByLength(cart.getProductsBill(), author);
        List<Book> expected = Arrays.asList(book2, book1, book, book3);

        assertEquals(expected, books);
    }
}