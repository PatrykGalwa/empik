package com.company.Sorter;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class AudioAlbumSorterTest {

    @Test
    @DisplayName("Should return product list filtered by audioalbum")
    void filterProductsByAudioAlbum() {
    }

    @Test
    @DisplayName("Should return product list filtered by audioalbum and sorted by published date")
    void filterProductsByAudioAlbumAndSortedByPublishedDate() {
    }
}