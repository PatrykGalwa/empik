package com.company;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.AudioAlbum.Track;
import com.company.Container.Book.PaperBook;
import com.company.Container.Game.Game;
import com.company.Container.Poster.Poster;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;


class CartTest {
    private Cart cart;
    private DiscountGenerator discountGenerator;
    private BigDecimal product1;
    private BigDecimal product2;
    private BigDecimal product3;

    @Test
    @DisplayName("Should make 10% discountGenerator each when book has more than or equals to 100 pages")

    public void shouldCalculateCurrentDiscountForBook() {
        cart = new Cart();
        discountGenerator = new DiscountGenerator(cart);

        PaperBook book = new PaperBook(1, "alala", BigDecimal.valueOf(300), "Ola", 100);
        PaperBook book1 = new PaperBook(1, "alala", BigDecimal.valueOf(200), "Ola", 100);
        PaperBook book2 = new PaperBook(1, "alala", BigDecimal.valueOf(100), "Ola", 100);


        cart.addProduct(book);
        cart.addProduct(book1);
        cart.addProduct(book2);


        product1 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(0));
        product2 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(1));
        product3 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(2));

        assertEquals(new BigDecimal(270), product1);
        assertEquals(new BigDecimal(180), product2);
        assertEquals(new BigDecimal(90), product3);
    }

    @Test
    @DisplayName("Should set price on 1 when have 3 posters in cart")

    public void shouldSetPriceOnOneWhenThreePostersInCart() {
        cart = new Cart();
        discountGenerator = new DiscountGenerator(cart);

        Poster poster = new Poster(1, "lklk", new BigDecimal(23));
        Poster poster1 = new Poster(1, "lklk", new BigDecimal(23));
        Poster poster2 = new Poster(1, "lklk", new BigDecimal(22));

        cart.addProduct(poster);
        cart.addProduct(poster1);
        cart.addProduct(poster2);

        product1 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(0));
        product2 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(1));
        product3 = discountGenerator.checkProductDiscount(cart.getProductsBill().get(2));

        assertEquals(new BigDecimal(23), product1);
        assertEquals(new BigDecimal(23), product2);
        assertEquals(new BigDecimal(1), product3);
    }

    @Test
    @DisplayName("Should make n% discountGenerator when past n years from published date")
    public void shouldMakeNPercentDiscountWhenPastNYearsFromPublishedDate() {
        cart = new Cart();
        discountGenerator = new DiscountGenerator(cart);
        List<Track> trackList = new ArrayList<>();
        LocalDate date = LocalDate.of(2009, 3,1);
        LocalDate date1 = LocalDate.of(2004, 3, 1);
        LocalDate date2 = LocalDate.of(1999, 3,1);

        AudioAlbum audioAlbum = new AudioAlbum(1, "lklk", new BigDecimal(100), date, trackList);
        AudioAlbum audioAlbum1 = new AudioAlbum(2, "lklk", new BigDecimal(150), date1, trackList);
        AudioAlbum audioAlbum2 = new AudioAlbum(3, "lklk", new BigDecimal(200), date2, trackList);

        product1 = discountGenerator.checkProductDiscount(audioAlbum);
        product2 = discountGenerator.checkProductDiscount(audioAlbum1);
        product3 = discountGenerator.checkProductDiscount(audioAlbum2);

        assertEquals(new BigDecimal(90), product1);
        assertEquals(new BigDecimal(127.5), product2);
        assertEquals(new BigDecimal(160), product3);
    }

    @Test
    @DisplayName("Should set the cheapest game price when three games with the same publisher exist in cast")

    public void shouldSetGamePriceOnZeroWhenThreeGamesWithTheSamePublisherInCast() {
        cart = new Cart();
        discountGenerator = new DiscountGenerator(cart);
        String publisher = "anna";
        Game game = new Game(1, "lklk", new BigDecimal(100), publisher);
        Game game1 = new Game(2, "klk", new BigDecimal(99), publisher);
        Game game2 = new Game(3, "lk", new BigDecimal(98), publisher);

        cart.addProduct(game);
        cart.addProduct(game1);
        cart.addProduct(game2);

        product1 = discountGenerator.checkProductDiscount(game);
        product2 = discountGenerator.checkProductDiscount(game1);
        product3 = discountGenerator.checkProductDiscount(game2);

        assertEquals(new BigDecimal(100), product1);
        assertEquals(new BigDecimal(99), product2);
        assertEquals(BigDecimal.ZERO, product3);
    }

    @Test
    @DisplayName("Should return Price after discountGenerator for all Products")

    public void shouldReturnBigDecimalAfterDiscountAllProductsWith10PercentFor100Each() {
        cart = new Cart();
        discountGenerator = new DiscountGenerator(cart);
        ReceiptGenerator receiptGenerator = new ReceiptGenerator();
        List<Track> trackList = new ArrayList<>();
        LocalDate date = LocalDate.of(2019, 3,1);
        LocalDate date1 = LocalDate.of(2019, 3, 1);
        LocalDate date2 = LocalDate.of(2019, 3,1);

        AudioAlbum audioAlbum = new AudioAlbum(1, "lklk", new BigDecimal(100), date, trackList);
        AudioAlbum audioAlbum1 = new AudioAlbum(2, "lklk", new BigDecimal(150), date1, trackList);
        AudioAlbum audioAlbum2 = new AudioAlbum(3, "lklk", new BigDecimal(200), date2, trackList);

        cart.addProduct(audioAlbum);
        cart.addProduct(audioAlbum1);
        cart.addProduct(audioAlbum2);

        BigDecimal priceAfterDiscount = receiptGenerator.printPriceAfterDiscount(cart.getProductsBill());

        assertEquals(new BigDecimal("430.0"), priceAfterDiscount);
    }
}