package com.company;

import com.company.Container.Product;

import java.math.BigDecimal;
import java.util.*;

public class Cart {
    final List<Product> productsBill;

    public Cart() {
        this.productsBill = new LinkedList<>();
    }

    public Cart(List<Product> productsBill) {
        this.productsBill = productsBill;
    }

    public void addProduct(Product product) {
        productsBill.add(product);
    }

    public void removeProductByName(Product product) {
        productsBill.remove(product);
    }

    public void removeProductByIndex(int index) {
        productsBill.remove(index);
    }

    public BigDecimal getCartValue() {
        return productsBill
                .stream()
                .map(product -> product.getPrice())
                .reduce(BigDecimal.ZERO, (sumPrice, actualPrice) -> sumPrice.add(actualPrice));
    }

    public List<Product> getProductsBill() {
        return productsBill;
    }
}