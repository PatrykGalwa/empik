package com.company;


import com.company.Container.Product;
import com.company.Repository.csv.Database.DatabaseFacade;
import com.company.Repository.csv.Database.DatabaseFacadeImpl;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {
        DatabaseFacade databaseFacade = new DatabaseFacadeImpl();
//        databaseFacade.addProduct(new Product("kokos", new BigDecimal(123)));

        Product p = databaseFacade.getProduct(1L);
        System.out.println(p);
        p.setName("kasztan");
        databaseFacade.update(p);
        System.out.println(p);
        databaseFacade.removeProduct(p);
        Product s = databaseFacade.getProduct(1L);
        System.out.println(s);
    }
}