package com.company;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.Book.PaperBook;
import com.company.Container.Game.Game;
import com.company.Container.Poster.Poster;
import com.company.Container.Product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

public class DiscountGenerator {
    private BigDecimal priceDiscounted;
    private List<Product> productsBill;
    private final BigDecimal baseDiscount = new BigDecimal(10);

    public DiscountGenerator(Cart cart) {
        this.productsBill = cart.productsBill;
    }

    public BigDecimal checkProductDiscount(Product product) {
        if (product instanceof PaperBook) {

            return checkPaperBookDiscount(product);

        } else if (product instanceof Poster) {

            return checkPosterDiscount(product);

        } else if (product instanceof AudioAlbum) {

            return checkAudioAlbumDiscount(product);

        } else if (product instanceof Game) {

            return checkGameDiscount(product);

        }
        return product.getPrice();
    }

    private BigDecimal checkPaperBookDiscount(Product product) {
        PaperBook book = (PaperBook) product;
        int count = book.getPageCounter()/100;
        BigDecimal multiply = new BigDecimal(count);
        BigDecimal maxDiscount = new BigDecimal(4);

//        List<PaperBook> books = productsBill.stream()
//                .filter(x -> x instanceof PaperBook)
//                .map(PaperBook.class::cast)
//                .collect(Collectors.toList());

        if(count >= 4) {
            priceDiscounted = product.getPrice().subtract(product.getPrice().divide(baseDiscount)
                    .multiply(maxDiscount));
        } else if (count <= 0){
            priceDiscounted = book.getPrice();
        } else if (count <= 3){
            priceDiscounted = product.getPrice().subtract(product.getPrice().divide(baseDiscount)
                    .multiply(multiply));
        }
        return priceDiscounted;
    }

    private BigDecimal checkPosterDiscount(Product product) {
        List<Poster> posters = productsBill.stream()
                .filter(x -> x instanceof Poster)
                .map(Poster.class::cast)
                .collect(Collectors.toList());

        int postersInCart = posters.size();
        int postersWithDiscount = postersInCart / 3;

        List<Poster> postersForDiscount = posters.stream()
                .sorted(Comparator.comparing(Poster::getPrice))
                .limit(postersWithDiscount)
                .collect(Collectors.toList());

        if(postersForDiscount.contains(product)) {
            priceDiscounted = BigDecimal.ONE;
        }else {
            priceDiscounted = product.getPrice();
        }
        return priceDiscounted;
    }

    private BigDecimal checkAudioAlbumDiscount(Product product) {
        AudioAlbum album = (AudioAlbum) product;
        long period = ChronoUnit.YEARS.between(album.getPublishedDate(), LocalDate.now());
        BigDecimal discountPrice = album.getPrice().divide(new BigDecimal(100))
                .multiply(new BigDecimal(period));

        return priceDiscounted = album.getPrice().subtract(discountPrice);
    }

    private BigDecimal checkGameDiscount(Product product) {
        Map<String, ArrayList<Game>> gamesGroupedByPublisher = productsBill.stream()
                .filter(x -> x instanceof Game)
                .map(Game.class::cast) //(x -> (Game)x
                .collect(
                        Collectors.toMap(
                                Game::getPublisher, //1 how create key
                                game -> new ArrayList<>(Collections.singletonList(game)), //Arrays.asList(); //2 how create value
                                (oldVal, newVal) -> {
                                    oldVal.addAll(newVal);
                                    return oldVal;
                                } //3 how resolve conflict (if no specified, just replace old value with new)
                        )
                );

        //alternativeMapForGames();

        Game game = (Game)product;

        ArrayList<Game> gamesOfPublisher = gamesGroupedByPublisher.get(game.getPublisher());
        int gamesWithDiscount = gamesOfPublisher.size() / 3;

        //get last x, where x - games with discount
        List<Game> gamesForDiscount = gamesOfPublisher.stream()
                .sorted(Comparator.comparing(Game::getPrice))
                .limit(gamesWithDiscount)
                .collect(Collectors.toList());

        //is product one of them ? if so, then price should be 0
        if (gamesForDiscount.contains(product)) {
            return BigDecimal.ZERO;
        } else {
            return product.getPrice();
        }
    }
}
