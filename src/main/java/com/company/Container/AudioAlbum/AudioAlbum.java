package com.company.Container.AudioAlbum;

import com.company.Container.Product;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class AudioAlbum extends Product {
    private final LocalDate publishedDate;
    private final List<Track> trackList;

    public AudioAlbum(long id, String name, BigDecimal price, LocalDate publishedDate, List<Track> trackList) {
        super(id, name, price);
        this.publishedDate = publishedDate;
        this.trackList = trackList;
    }

    public LocalDate getPublishedDate() {
        return publishedDate;
    }

    public List<Track> getTrackList() {
        return trackList;
    }
}
