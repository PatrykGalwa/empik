package com.company.Container.Book;

import java.math.BigDecimal;
import java.util.List;

public class EBook extends Book {
    private final List<String> supportedFormats;

    public EBook(long id, String name, BigDecimal price, String author, int pageCounter, List<String> supportedFormats) {
        super(id, name, price, author, pageCounter);
        this.supportedFormats = supportedFormats;
    }

    public List<String> getSupportedFormats() {
        return supportedFormats;
    }
}
