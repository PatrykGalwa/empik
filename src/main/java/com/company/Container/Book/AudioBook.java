package com.company.Container.Book;

import java.math.BigDecimal;

public class AudioBook extends Book {
    private final int duration;

    public AudioBook(long id, String name, BigDecimal price, String author, int pageCounter, int duration) {
        super(id, name, price, author, pageCounter);
        this.duration = duration;
    }

    public int getDuration() {
        return duration;
    }
}
