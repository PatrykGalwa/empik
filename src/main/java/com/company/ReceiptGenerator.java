package com.company;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.Book.Book;
import com.company.Container.Game.Game;
import com.company.Container.Product;
import com.company.Repository.api.nbp.ExchangeRates;
import com.company.Repository.api.nbp.NBPRepository;

import java.math.BigDecimal;
import java.util.List;

public class ReceiptGenerator {
    Cart cart;

    public void printBill(List<Product> productsBill) {
        printBill(productsBill, "PLN");
    }

    public void printBill(List<Product> productsBill, String currency) {
        cart = new Cart(productsBill);
        DiscountGenerator discountGenerator = new DiscountGenerator(cart);
        String formatter = "%2s| %-40s| %5s| %20s";
        String endFormatter = "%44s| %5s| %20s";
        String nameField = "";
        BigDecimal fullPrice;

        System.out.println(String.format(formatter, "ID", "NAME", "PRICE", "PRICE AFTER DISCOUNT"));

        for (int i = 0; i < productsBill.size(); i++) {

            if (productsBill.get(i) instanceof Game) {

                nameField = productsBill.get(i).getName() + ", " + ((Game) productsBill.get(i)).getPublisher();

            } else if (productsBill.get(i) instanceof AudioAlbum) {

                nameField = productsBill.get(i).getName() + ", " + ((AudioAlbum) productsBill.get(i)).getPublishedDate();

            } else if (productsBill.get(i) instanceof Book) {

                nameField = productsBill.get(i).getName() + ", " + ((Book) productsBill.get(i)).getAuthor();

            } else {

                nameField = productsBill.get(i).getName();
            }

            System.out.println(String.format(formatter,  productsBill.get(i).getId(),
                    nameField, displayValueInOthCurrency(currency, productsBill.get(i).getPrice()),
                    displayValueInOthCurrency(currency, discountGenerator.checkProductDiscount(productsBill.get(i)))));
        }
        fullPrice = displayValueInOthCurrency(currency, printPriceAfterDiscount(productsBill));

        System.out.println(String.format(endFormatter, checkCurrencyExist(currency),
                displayValueInOthCurrency(currency, cart.getCartValue()),fullPrice));
    }

    public BigDecimal printPriceAfterDiscount(List<Product> productsBill) {
        cart = new Cart(productsBill);
        BigDecimal sumAfterDiscount = new BigDecimal(0);

        for (int i = 0; i < productsBill.size(); i++) {
            sumAfterDiscount = sumAfterDiscount.add(new DiscountGenerator(cart).checkProductDiscount(productsBill.get(i)));
        }

        int discount = sumAfterDiscount.divide(new BigDecimal(100)).compareTo(new BigDecimal(1));

        if (discount >= 1) {
            BigDecimal restFromDividingByHundred = sumAfterDiscount.remainder(new BigDecimal(100));
            BigDecimal fullHundreds = sumAfterDiscount.subtract(restFromDividingByHundred);
            sumAfterDiscount = fullHundreds.subtract(fullHundreds.divide(new BigDecimal(20)));
            sumAfterDiscount = sumAfterDiscount.add(restFromDividingByHundred);
        }
        return sumAfterDiscount;
    }

    private BigDecimal displayValueInOthCurrency(String currency, BigDecimal priceAfterDiscount) {

        NBPRepository nbpRepository = new NBPRepository();

        List<ExchangeRates> response = nbpRepository.getResponse();
        double rate = response.get(0).getRates().stream()
                .filter(x -> x.getCode().equals(currency))
                .map(x -> x.getAsk())
                .findFirst().orElse(1.0);

        return priceAfterDiscount.divide(new BigDecimal(rate), 2, BigDecimal.ROUND_HALF_UP);
    }

    private String checkCurrencyExist(String currency) {
        NBPRepository nbpRepository = new NBPRepository();
        String actualCurrency = "";

        List<ExchangeRates> response = nbpRepository.getResponse();
        boolean rate = response.get(0).getRates().stream()
                .anyMatch(x -> x.getCode().equals(currency));

        if (rate) {
            actualCurrency = currency;
        } else {
            actualCurrency = "PLN";
        }
        return actualCurrency;
    }
}