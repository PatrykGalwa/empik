package com.company.Sorter;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.Product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class AudioAlbumSorter {

    public List<AudioAlbum> filterProductsByAudioAlbum(List<Product> products) {
        return products.stream()
                .filter(x -> x instanceof AudioAlbum)
                .map(AudioAlbum.class::cast)
                .collect(Collectors.toList());
    }

    public List<AudioAlbum> filterProductsByAudioAlbumAndSortedByPublishedDate(List<Product> products) {
        return filterProductsByAudioAlbum(products).stream()
                .sorted(Comparator.comparing(x -> x.getPublishedDate()))
                .collect(Collectors.toList());
    }
}
