package com.company.Sorter;

import com.company.Container.Poster.Poster;

import java.util.List;
import java.util.stream.Collectors;

public class PosterSorter {

    public List<Poster> filterProductListByPosters(List<Poster> posters) {
        return posters.stream()
                .filter(x -> x instanceof Poster)
                .map(Poster.class::cast)
                .collect(Collectors.toList());
    }
}
