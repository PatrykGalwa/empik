package com.company.Sorter;

import com.company.Container.Game.Game;
import com.company.Container.Product;

import java.util.List;
import java.util.stream.Collectors;

public class GameSorter {

    public List<Game> filterProductListByGames(List<Product> products) {
        return products.stream()
                .filter(x -> x instanceof Game)
                .map(Game.class::cast)
                .collect(Collectors.toList());
    }

    public List<Game> filterProductsByGamesAndPublisher(List<Product> products, String publisher) {
        return filterProductListByGames(products).stream()
                .filter(x -> x.getPublisher().equals(publisher))
                .collect(Collectors.toList());
    }
}
