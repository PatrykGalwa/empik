package com.company.Sorter;

import com.company.Container.Product;

import java.math.BigDecimal;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class ProductSorter {

    public List<Product> filterProductListSortedByPrice(List<Product> products) {
        return products.stream()
                .sorted(Comparator.comparing(x -> x.getPrice()))
                .collect(Collectors.toList());
    }

    public List<Product> filterProductListSortedByName(List<Product> products) {
        return products.stream()
                .sorted(Comparator.comparing(x -> x.getName()))
                .collect(Collectors.toList());
    }

    public List<Product> sortProductListByPriceandFilterByName(List<Product> products, BigDecimal price) {
        return products.stream()
                .sorted(Comparator.comparing(x -> x.getPrice()))
                .filter(x -> x.getPrice().equals(price))
                .collect(Collectors.toList());
    }
}