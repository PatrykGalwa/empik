package com.company.Sorter;

import com.company.Container.Book.Book;
import com.company.Container.Product;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class BookSorter {
    public List<Book> filterProductsByBooks(List<Product> products) {
        return products.stream()
                .filter(book -> book instanceof Book)
                .map(Book.class::cast)
                .collect(Collectors.toList());
    }

    public List<Book> filterProductsByBooksAndAuthor(List<Product> products, String author) {
        return filterProductsByBooks(products).stream()
                .filter(x -> x.getAuthor().equals(author))
                .collect(Collectors.toList());
    }

    public List<Book> filterProductsByBooksAndSortedByLength(List<Product> products) {
        return filterProductsByBooks(products).stream()
                .sorted(Comparator.comparing(x -> x.getPageCounter()))
                .collect(Collectors.toList());
    }

    public List<Book> filterProductsByBookAuthorAndSortedByLength(List<Product> products, String author) {
        return filterProductsByBooksAndAuthor(products, author).stream()
                .sorted(Comparator.comparing(x -> x.getPageCounter()))
                .collect(Collectors.toList());
    }
}
