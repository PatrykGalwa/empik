package com.company.Repository.api.nbp;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class ExchangeRates {
    private String table;
    private String no;
    private String tradingDate;
    private String effectiveDate;
    private List<SingleRate> rates;

    public ExchangeRates(@JsonProperty("table") String table,
                         @JsonProperty("no") String no,
                         @JsonProperty("tradingDate") String tradingDate,
                         @JsonProperty("effectiveDate") String effectiveDate,
                         @JsonProperty("rates") List<SingleRate> rates) {
        this.table = table;
        this.no = no;
        this.tradingDate = tradingDate;
        this.effectiveDate = effectiveDate;
        this.rates = rates;
    }

    public String getTable() {
        return table;
    }

    public String getTradingDate() {
        return tradingDate;
    }

    public String getEffectiveDate() {
        return effectiveDate;
    }

    public List<SingleRate> getRates() {
        return rates;
    }

    @Override
    public String toString() {
        return "ExchangeRates{" +
                "table='" + table + '\'' +
                ", no='" + no + '\'' +
                ", tradingDate='" + tradingDate + '\'' +
                ", effectiveDate='" + effectiveDate + '\'' +
                ", rates=" + rates +
                '}';
    }
}