package com.company.Repository.api.nbp;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

public class SingleRate {
    private String currency;
    private String code;
    private Double bid;
    private Double ask;

    @JsonCreator
    public SingleRate(@JsonProperty("currency") String currency,
                      @JsonProperty("code") String code,
                      @JsonProperty("bid") Double bid,
                      @JsonProperty("ask") Double ask) {
        this.currency = currency;
        this.code = code;
        this.bid = bid;
        this.ask = ask;
    }

    public String getCurrency() {
        return currency;
    }

    public String getCode() {
        return code;
    }

    public Double getBid() {
        return bid;
    }

    public Double getAsk() {
        return ask;
    }

    @Override
    public String toString() {
        return "SingleRate{" +
                "currency='" + currency + '\'' +
                ", code='" + code + '\'' +
                ", bid=" + bid +
                ", ask=" + ask +
                '}';
    }
}
