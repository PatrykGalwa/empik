package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Book.PaperBook;
import com.company.Container.Product;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;

public class CSVPaperBook implements CSVProductEntity {

    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @CsvBindByName(column = "author", required = true)
    private String author;

    @CsvBindByName(column = "pageCounter", required = true)
    private Integer pageCounter;

    @Override
    public Product toProduct() {
        return new PaperBook(id, name, new BigDecimal(price), author, pageCounter);
    }
}
