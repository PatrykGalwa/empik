package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Poster.Poster;
import com.company.Container.Product;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;

public class CSVPoster implements CSVProductEntity{

    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @Override
    public Product toProduct() {
        return new Poster(id, name, new BigDecimal(price));
    }
}
