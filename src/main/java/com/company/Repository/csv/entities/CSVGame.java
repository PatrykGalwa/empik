package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Game.Game;
import com.company.Container.Product;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;


public class CSVGame implements CSVProductEntity{

    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @CsvBindByName(column = "publisher", required = true)
    private String publisher;

    @Override
    public Product toProduct() {
        return new Game(id, name, new BigDecimal(price), publisher);
    }
}
