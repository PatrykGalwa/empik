package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.opencsv.bean.CsvBindByName;

public class CSVTrack{

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "duration", required = true)
    private Long duration;

    public Track toProduct() {
        return new Track(name, duration);
    }
}
