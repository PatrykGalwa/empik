package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Product;

public interface CSVProductEntity {
    Product toProduct();
}