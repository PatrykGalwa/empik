package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Book.AudioBook;
import com.company.Container.Product;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;

public class CSVAudioBook implements CSVProductEntity{
    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @CsvBindByName(column = "author", required = true)
    private String author;

    @CsvBindByName(column = "pageCounter", required = true)
    private Integer pageCounter;

    @CsvBindByName(column = "duration", required = true)
    private Integer duration;

    @Override
    public Product toProduct() {
        return new AudioBook(id, name, new BigDecimal(price), author, pageCounter, duration);
    }
}
