package com.company.Repository.csv.entities;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.AudioAlbum.Track;
import com.company.Container.Product;
import com.opencsv.bean.CsvBindByName;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.List;

public class CSVAudioAlbum implements CSVProductEntity{

    @CsvBindByName(column = "id", required = true)
    private Long id;

    @CsvBindByName(column = "name", required = true)
    private String name;

    @CsvBindByName(column = "price", required = true)
    private String price;

    @CsvBindByName(column = "publishedDate", required = true)
    private String publishedDate;

    @CsvBindByName(column = "trackList", required = true)
    private List<Track> trackList;

    @Override
    public Product toProduct() {
        return new AudioAlbum(id, name, new BigDecimal(price), LocalDate.parse(publishedDate), trackList);
    }
}