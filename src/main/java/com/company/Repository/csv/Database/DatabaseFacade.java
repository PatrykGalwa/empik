package com.company.Repository.csv.Database;

import com.company.Container.Product;

import java.util.Collection;
import java.util.List;

public interface DatabaseFacade {
    Product getProduct(long id);
    List<? extends Product> getAllProducts();
    void addProduct(Product product);
    void addManyProducts(Collection<Product> products);
    void update(Product updatedProduct);
    void removeProduct(Product product);
}