package com.company.Repository.csv.Database;

import com.company.Container.Product;


import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class DatabaseFacadeImpl implements DatabaseFacade {
    private Connection conn;
    private Statement statement;
    private ResultSet resultSet;
    private int result;
    private String query;

    @Override
    public Product getProduct(long id) {
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "SELECT * FROM products WHERE id = ?";
            statement = conn.prepareStatement(query);
            ((PreparedStatement) statement).setLong(1, id);

            resultSet = ((PreparedStatement)statement).executeQuery();
            while (resultSet.next()) {
                return new Product(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getBigDecimal(3));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<? extends Product> getAllProducts() {
        List<Product> products = new ArrayList<>();
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "SELECT * FROM products";
            statement = conn.createStatement();

            resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                products.add(new Product(
                        resultSet.getLong(1),
                        resultSet.getString(2),
                        resultSet.getBigDecimal(3)));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return products;
    }

    @Override
    public void addProduct(Product product) {
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "INSERT INTO products(product_name, price) VALUES (?, ?)";
            statement = conn.prepareStatement(query);
            ((PreparedStatement) statement).setString(1, product.getName());
            ((PreparedStatement) statement).setBigDecimal(2, product.getPrice());

            result = ((PreparedStatement) statement).executeUpdate();

            if (result == 0) {
                throw new IllegalStateException("Insertion fail " + product);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void addManyProducts(Collection<Product> products) {
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "INSERT INTO products(product_name, price) VALUES (?, ?)";
            for (Product product : products) {
                statement = conn.prepareStatement(query);
                ((PreparedStatement) statement).setString(1, product.getName());
                ((PreparedStatement) statement).setBigDecimal(2, product.getPrice());

                result = ((PreparedStatement) statement).executeUpdate();
                if (result == 0) {
                    throw new IllegalStateException("Insertion fail " + product);
                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void update(Product updatedProduct) {
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "UPDATE products SET product_name = ?, price = ? where id = ?";
            statement = conn.prepareStatement(query);
            ((PreparedStatement) statement).setString(1, updatedProduct.getName());
            ((PreparedStatement) statement).setBigDecimal(2, updatedProduct.getPrice());
            ((PreparedStatement) statement).setLong(3, updatedProduct.getId());

            result = ((PreparedStatement) statement).executeUpdate();
            if (result == 0) {
                throw new IllegalStateException("Update fail " + updatedProduct);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void removeProduct(Product product) {
        try {
            conn = DBUtil.getInstance().getConnection();
            query = "DELETE FROM products WHERE id = ?";
            statement = conn.prepareStatement(query);
            ((PreparedStatement) statement).setLong(1, product.getId());

            result = ((PreparedStatement) statement).executeUpdate();
            if (result == 0) {
                throw new IllegalStateException("Delete fail " + product);
            }
        } catch(SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                conn.close();
                statement.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}