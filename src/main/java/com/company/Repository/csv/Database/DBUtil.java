package com.company.Repository.csv.Database;

import com.mchange.v2.c3p0.ComboPooledDataSource;

import java.beans.PropertyVetoException;
import java.sql.Connection;
import java.sql.SQLException;

public class DBUtil {
    private static DBUtil dbUtil;
    private ComboPooledDataSource comboPooledDataSource;
    private String login = "root";
    private String password = "Spodnie12";

    private DBUtil() throws PropertyVetoException {
        comboPooledDataSource = new ComboPooledDataSource();

        comboPooledDataSource.setDriverClass("com.mysql.cj.jdbc.Driver");
        comboPooledDataSource.setJdbcUrl("jdbc:mysql://localhost:3306/product?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC");
        comboPooledDataSource.setUser(login);
        comboPooledDataSource.setPassword(password);

        comboPooledDataSource.setInitialPoolSize(5);
        comboPooledDataSource.setMaxPoolSize(20);
        comboPooledDataSource.setMinPoolSize(1);
        comboPooledDataSource.setAcquireIncrement(5);
        comboPooledDataSource.setMaxIdleTime(3600);
    }

    public Connection getConnection() throws SQLException {
        return comboPooledDataSource.getConnection();
    }

    public static DBUtil getInstance() {
        if (dbUtil == null) {
            try {
                dbUtil = new DBUtil();
            }catch (PropertyVetoException e) {
                e.printStackTrace();
            }
        }
        return dbUtil;
    }

    public boolean checkPassword(String login, String password) {
        return this.login.equals(login) && this.password.equals(password);
    }
}
