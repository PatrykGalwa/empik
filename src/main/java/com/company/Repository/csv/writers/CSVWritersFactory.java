package com.company.Repository.csv.writers;

import com.company.Container.Book.PaperBook;
import com.company.Container.Product;
import com.company.Repository.csv.writers.productWriters.*;

import java.util.HashMap;
import java.util.Map;

public class CSVWritersFactory {

    private static Map<Class, ProductToCSVWriter> writers;

    static {
        writers = new HashMap<>();
        writers.put(PaperBook.class, new PaperBookToCSVWriter());
    }

    public static ProductToCSVWriter baseOnProduct(Product product){
        if(writers.containsKey(product.getClass())){
            return writers.get(product.getClass());
        }
        throw new IllegalArgumentException("no suitable ProductToCSVWriter for class " + product.getClass().getName());
    }
}

