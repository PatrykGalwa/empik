package com.company.Repository.csv.writers.productWriters;

import com.company.Container.Book.AudioBook;
import com.company.Container.Product;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class AudioBookToCSVWriter implements ProductToCSVWriter {

    final static String FILE_NAME = "audiobooks";

    public AudioBookToCSVWriter() {
    }

    @Override
    public void saveProducts(Collection<Product> products, String databaseName) {
        try {
            File file = ProductToCSVWriter.super.openOrCreateFile(databaseName
                    .concat(File.separator)
                    .concat(FILE_NAME)
                    .concat(".csv"));

            Writer writer = new FileWriter(file);
            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            List<AudioBook> beansToSave = products.stream().map(x -> (AudioBook) x).collect(Collectors.toList());
            beanToCsv.write(beansToSave);
            writer.close();
        } catch (CsvDataTypeMismatchException | IOException | CsvRequiredFieldEmptyException e) {
            e.printStackTrace();
        }
    }
}