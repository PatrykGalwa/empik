package com.company.Repository.csv.writers.productWriters;

import com.company.Container.Product;
import com.company.Repository.csv.CSVDatabaseFile;

import java.util.Collection;

public interface ProductToCSVWriter extends CSVDatabaseFile {

    void saveProducts(Collection<Product> products, String databaseName);
}
