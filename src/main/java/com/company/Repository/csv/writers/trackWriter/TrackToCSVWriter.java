package com.company.Repository.csv.writers.trackWriter;

import com.company.Container.AudioAlbum.Track;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;

public class TrackToCSVWriter {
    final static String FILE_NAME = "tracks";
    private final String databaseNameFile;

    public TrackToCSVWriter(String databaseName) {
        this.databaseNameFile = databaseName;
    }

    public void saveTracks(Collection<Track> tracks) {
        try {
            File file = new File(databaseNameFile.concat(File.separator).concat(FILE_NAME).concat(".csv"));
            Writer writer = new FileWriter(file);

            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            beanToCsv.write(tracks);
            writer.close();
        } catch (CsvRequiredFieldEmptyException | IOException | CsvDataTypeMismatchException e) {
            e.printStackTrace();
        }
    }
}
