package com.company.Repository.csv.writers.productWriters;

import com.company.Container.Game.Game;
import com.company.Container.Product;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import com.opencsv.exceptions.CsvDataTypeMismatchException;
import com.opencsv.exceptions.CsvRequiredFieldEmptyException;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Writer;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class GameToCSVWriter implements ProductToCSVWriter {
    final static String FILE_NAME = "games";


    public GameToCSVWriter(String databaseName) {

    }

    @Override
    public void saveProducts(Collection<Product> products, String databaseName) {
        try {
            File file = new File(databaseName
                    .concat(File.separator)
                    .concat(FILE_NAME)
                    .concat(".csv"));
            Writer writer = new FileWriter(file);

            StatefulBeanToCsv beanToCsv = new StatefulBeanToCsvBuilder(writer).build();
            List<Game> beansToSave = products.stream().map(x -> (Game) x).collect(Collectors.toList());
            beanToCsv.write(beansToSave);
            writer.close();
        }catch (CsvRequiredFieldEmptyException | IOException | CsvDataTypeMismatchException e){
            e.printStackTrace();
        }
    }
}
