package com.company.Repository.csv;

import com.company.Container.Product;
import com.company.Repository.csv.Database.DatabaseFacade;
import com.company.Repository.csv.readers.CSVReadersFactory;
import com.company.Repository.csv.readers.productReaders.CSVToProductReader;
import com.company.Repository.csv.writers.CSVWritersFactory;

import java.io.IOException;
import java.util.*;

public class CSVDatabase implements DatabaseFacade {

    private Collection<CSVToProductReader> readers;
    private String databaseName;

    public CSVDatabase(String databaseName) throws IOException {
        this.databaseName = databaseName;
        readers = CSVReadersFactory.getAllReaders();
    }


    @Override
    public Product getProduct(long id) {
        try {
            return getAllProducts().stream()
                    .filter(x -> x.getId() == id)
                    .findFirst()
                    .get();

        } catch (NoSuchElementException e){
            System.out.println("element not found");
            return null;
        }
    }

    @Override
    public List<Product> getAllProducts() {
        List<Product> results = new ArrayList<>();
        for(CSVToProductReader provider : readers){
            results.addAll(provider.getProducts(databaseName));
        }
        return results;
    }

    @Override
    public void addProduct(Product product) {

        CSVWritersFactory.baseOnProduct(product);

    }

    @Override
    public void addManyProducts(Collection<Product> products) {
        for (Product product : products) {
            addProduct(product);
        }
    }

    @Override
    public void update(Product updatedProduct) {

    }

    @Override
    public void removeProduct(Product product) {

    }
}
