package com.company.Repository.csv.readers.trackReaders;

import com.company.Container.AudioAlbum.Track;
import com.company.Container.Product;
import com.company.Repository.csv.CSVDatabaseFile;
import com.company.Repository.csv.entities.CSVAudioAlbum;
import com.company.Repository.csv.entities.CSVTrack;
import com.company.Repository.csv.readers.productReaders.CSVToProductReader;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CSVToTrackReader {

    private final static String FILE_NAME = "tracks";
    public Collection<Track> getProducts(String databasePath) {
        try {
            File file = openOrCreateFile(databasePath.concat(File.separator).concat(FILE_NAME)
                    .concat(".csv"));
            System.out.println(file.getAbsolutePath());

            CsvToBean<CSVTrack> csvToBean = new CsvToBeanBuilder<CSVTrack>(new FileReader(file))
                    .withType(CSVTrack.class)
                    .build();

            List<Track> results = csvToBean.parse().stream()
                    .map(CSVTrack::toProduct)
                    .collect(Collectors.toList());

            return results;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return Collections.emptyList();
    }
    private File openOrCreateFile(String databaseFilePath) {
        File file = new File(databaseFilePath);
        File parent = file.getParentFile();
        if (!parent.exists() && !parent.mkdirs()) {
            throw new IllegalStateException("Couldn't create dir: " + parent);
        }
        if (!file.exists()) {
            try {
                boolean newFile = file.createNewFile();
                System.out.println(newFile);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return file;
    }
}
