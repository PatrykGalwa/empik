package com.company.Repository.csv.readers.productReaders;

import com.company.Container.Product;
import com.company.Repository.csv.entities.CSVAudioAlbum;
import com.opencsv.bean.CsvToBean;
import com.opencsv.bean.CsvToBeanBuilder;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

public class CSVToAudioAlbumReader implements CSVToProductReader {

    private static final String FILE_NAME = "audioalbums";

    public Collection<Product> getProducts(String databasePath) {
        try {
            File file = CSVToProductReader.super.openOrCreateFile(databasePath
                    .concat(File.separator)
                    .concat(FILE_NAME)
                    .concat(".csv"));
            System.out.println(file.getAbsolutePath());

            CsvToBean<CSVAudioAlbum> csvToBean = new CsvToBeanBuilder<CSVAudioAlbum>(new FileReader(file))
                    .withType(CSVAudioAlbum.class)
                    .build();

            List<Product> results = csvToBean.parse().stream()
                    .map(CSVAudioAlbum::toProduct)
                    .collect(Collectors.toList());

            return results;

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return Collections.emptyList();
    }
}
