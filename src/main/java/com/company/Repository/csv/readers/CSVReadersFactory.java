package com.company.Repository.csv.readers;

import com.company.Container.AudioAlbum.AudioAlbum;
import com.company.Container.Book.AudioBook;
import com.company.Container.Book.EBook;
import com.company.Container.Book.PaperBook;
import com.company.Container.Game.Game;
import com.company.Container.Poster.Poster;
import com.company.Container.Product;
import com.company.Repository.csv.readers.productReaders.*;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

public class CSVReadersFactory {

    private static Map<Class, CSVToProductReader> readers;

    static {
        readers = new HashMap<>();
        readers.put(PaperBook.class, new CSVToPaperBookReader());
        readers.put(AudioAlbum.class, new CSVToAudioAlbumReader());
        readers.put(AudioBook.class, new CSVToAudioBookReader());
        readers.put(EBook.class, new CSVToEBookReader());
        readers.put(Game.class, new CSVToGameReader());
        readers.put(Poster.class, new CSVToPosterReader());
    }

    public static CSVToProductReader baseOnProductType(Product product){
        if(readers.containsKey(product.getClass())){
            return readers.get(product.getClass());
        }
        throw new IllegalArgumentException("no suitable CSVReader for class " + product.getClass().getName());
    }

    public static Collection<CSVToProductReader> getAllReaders(){
        return readers.values();
    }
}