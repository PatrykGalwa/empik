package com.company.Repository.csv.readers.productReaders;

import com.company.Container.Product;
import com.company.Repository.csv.CSVDatabaseFile;

import java.util.Collection;

public interface CSVToProductReader extends CSVDatabaseFile {
    Collection<Product> getProducts(String dataBase);
}